<?php

Class Barang {

    private $id;
    private $nama;
    private $qty;

    public function __construct()
    {
        $this->id = "B01";
        $this->nama = "Motherboard";
        $this->qty = "40";
    }

    public function getData() {
        return "Data dari model barang : $this->nama, $this->id, $this->qty ";
    }

    public function getDataOne() {
        $data = [
            'id'    =>$this->id,
            'nama'    =>$this->nama,
            'qty'    =>$this->qty,
        ];
        return $data;
    }
}